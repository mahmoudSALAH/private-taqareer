package com.Battalion.kartella;

import org.json.JSONException;
import org.json.JSONObject;

public class user {
	public String user_id;
	public  String user_name;
	public String user_lat;
	public String user_lng;
	public String user_datetime;
	
	public user(JSONObject json) {
		try {
			this.user_id = json.getString("user_id");
			this.user_name = json.getString("user_name");
			this.user_lat = json.getString("user_lat");
			this.user_lng = json.getString("user_lng");
			this.user_datetime = json.getString("user_time");
						
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
